
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.*;
import java.awt.event.*;
import java.lang.Math;
import java.awt.*;


class ColorChange{
	
	
	private Color cl;
	private String identity;
	
	public ColorChange(Color cl, String identity){
		this.cl = cl;
		this.identity = identity;
	}
	
	public String toString()
	{ 
		return identity; }
	
	public Color getColor()
	{ 
		return cl; }
}



public class Graphplot extends JPanel implements ActionListener, AdjustmentListener {


	private int padding = 40;
	private int labelpadding = 30;
	
	private Color lineClr = new Color(44, 102, 230, 180);
    private Color gridClr = new Color(200, 200, 200, 200);
	
	private List<Double> temp;
	private JButton bu1;
	private JTextField status;

	private Scrollbar scrollb;

	private JComboBox<ColorChange> cmbx;
	private JCheckBox cbx;
	private JTextArea ta;
	

	
	public Graphplot(List<Double> temp) {
		this.temp = temp;
        this.setLayout(new FlowLayout());
        status = new JTextField(20);
        this.add(status);
        
        this.bu1 = new JButton("clear");
        this.bu1.addActionListener(this);
        this.add("Center",this.bu1);
      
        
        JPanel ctrls = new JPanel(new FlowLayout());	
		
		  JCheckBox cbx = new JCheckBox("Read-Only");
		  cbx.addActionListener(this);
		  ctrls.add(cbx);

		  ctrls.add(new JLabel(" Color of Graph:"));
		  ColorChange[] items = { new ColorChange (Color.yellow, "YELLOW"), 
				  new ColorChange (Color.green, "Green"),
				  new ColorChange (Color.red, "Red"),
				  new ColorChange (Color.black, "BLACK"),
				  new ColorChange (Color.blue, "Blue")};
		  cmbx = new JComboBox<ColorChange>(items);
		  cmbx.addActionListener(this);
		  ctrls.add(cmbx);
        this.add(ctrls);
        
        this.scrollb= new Scrollbar(Scrollbar.HORIZONTAL, 50, 10, 0, 20);
        this.scrollb.addAdjustmentListener(this);
        this.add(scrollb);}


        public void adjustmentValueChanged(AdjustmentEvent e){
            if (e.getSource().equals(scrollb)){
                    this.updateUI();
            }
		 
		  JTextField update = new JTextField();
		  this.add( null, "Application Started.");

		  // main area, with scrolling and wrapping
		  this.ta = new JTextArea(20,40);
		  JScrollPane p = new JScrollPane(this.ta,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		  this.ta.setWrapStyleWord(true);

	
        this.setVisible(true);
	}
	

	public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("clear")) {
          this.status.setText("");
        }
	}
	protected void paintComponent (Graphics g) {
		super.paintComponent(g);
		double xScal= ((double)getWidth()-( 3*padding))/(temp.size());
		double yScal= ((double)getHeight()-( 3*padding))/(getMaxTemperature() -getMinTemperature());
		
			List<Point> graphPoint = new ArrayList<>();
			for(int j=0; j<temp.size();j++) {
				int xa = (int) (j *xScal+padding);   // Graph representation
				int ya = (int) ((getMaxTemperature() - temp.get(j)) * yScal) ;
			graphPoint.add(new Point(xa, ya));	
				
		}
		
		// draw background
        g.setColor(Color.YELLOW);
        g.fillRect(padding + labelpadding, padding, getWidth() , getHeight());
        g.setColor(Color.RED);
        
   

        // For x axis
        for (int j = 0; j < temp.size(); j++) {
            if (temp.size() > 1) {
                int s0 = j * (getWidth() - padding- labelpadding) ;
                int s1 = s0;
                int t0 = getHeight() - padding - labelpadding;
                int t1 = t0;
                if ((j % ((temp.size() / 20.0)) == 0)) {
                    g.setColor(gridClr);
                    g.drawLine(s0,s1, t0, t1);
                    g.setColor(Color.RED);
                  
                }
            }
        }
        g.setColor(lineClr);
        for (int j = 0; j < graphPoint.size() - 1; j++) {
            int s1 = graphPoint.get(j).x;
            int t1 = graphPoint.get(j).y;
            int s2 = graphPoint.get(j + 1).x;
            int t2 = graphPoint.get(j + 1).y;
            g.drawLine(s1, t1, s2, t2);
        }

	}
	
private double getMinTemperature() {   
    double min_temp = Double.MAX_VALUE;
    for (Double temper : temp) {
        min_temp = Math.min(min_temp, temper);
    }
    return min_temp;
}

private double getMaxTemperature() {
    double max_temp = Double.MIN_VALUE;
    for (Double temper : temp) {
        max_temp = Math.max(max_temp, temper);
    }
    return max_temp;
}
		
public void setTemp(List<Double> temp) {
    this.temp = temp;
    this.repaint();
}

public List<Double> getTemp() {
    return temp;
} 
	 
private static void Showtemp() {
    List<Double> temp = new ArrayList<>();
    Random random = new Random();
    int maxPoints = 40;
    int maxtemp = 10;
    for (int j = 0; j < maxPoints; j++) {
        temp.add((double) random.nextDouble() * maxtemp);
    }
    Graphplot mainPlot = new Graphplot(temp);
    mainPlot.setPreferredSize(new Dimension(850, 500));
    JFrame frame = new JFrame("Graph");   //Display graph
   
    frame.getContentPane().add(mainPlot);
    frame.pack();
frame.setVisible(true);
    
}

public static void main(String[] args) {
 Showtemp();
}
}




