*  Temperature sensor client/server application, where the Raspberry Pi reads the CPU core temperature from a file system entry and sends the data to a desktop computer client.
*  The desktop PC is the graphical temperature server. It runs on a server port on the PC and the RPi boards connect to it.
*  When a client application executes on the RPi it will connect to the server that is running on the PC and transfer temperature data to the server.
*  The Server will then display the data in a Graphical User Interface (GUI).